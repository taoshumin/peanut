/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package peanut

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"net"
	"net/http"

	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/peerstore"
)

// httpClient Client sends http proxy request service.
// L7->l4
type httpClient struct {
	host host.Host
	pid  PeerInfo
	pro  Protocol

	listener net.Listener
	ctx      context.Context
	logger   Logger

	address string
}

func NewHttpService(
	ctx context.Context,
	host host.Host,
	pid PeerInfo,
	pro Protocol,
	address string,
	logger Logger) *httpClient {
	return &httpClient{
		ctx:     ctx,
		host:    host,
		pid:     pid,
		pro:     pro,
		address: address,
		logger:  logger,
	}
}

func (h *httpClient) Open() error {
	ln, err := net.Listen("tcp", h.address)
	if err != nil {
		return err
	}
	h.listener = ln
	go func() {
		if err := http.Serve(ln, h); err != nil {
			h.logger.
				Fatal(fmt.Errorf("http server start err: %s", err))
		}
	}()

	// join pid into libp2p.
	h.host.Peerstore().AddAddr(h.pid.PeerID, h.pid.Address, peerstore.PermanentAddrTTL)

	h.logger.
		WithField("address", h.address).
		Info("service started successfully")
	return err
}

func (h *httpClient) WithLogger(log Logger) {
	h.logger = log.
		WithField("service", "client").
		WithField("peerID", h.pid.PeerID.Pretty())
}

func (h *httpClient) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.logger.Info("request url from proxy ", r.URL)
	// NewStream opens a new stream to given peer p, and writes a p2p/protocol
	// header with given ProtocolID. If there is no connection to p, attempts
	// to create one. If ProtocolID is "", writes no header.
	stream, err := h.host.NewStream(h.ctx, h.pid.PeerID, h.pro.PID())
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	// Reset closes both ends of the stream. Use this to tell the remote
	// side to hang up and go away.
	defer stream.Reset()
	// Closer is the interface that wraps the basic Close method.
	defer stream.Close()

	// get data from http request and write request data to stream.
	// body empty will return error.
	// writes the HTTP request to the stream.
	// Stream Can read and write data to remote peer. (Current Write)
	if err := r.Write(stream); err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// Now we read the response that was sent from the dest.
	// Stream Can read and write data to remote peer. (Current Read)
	body := bufio.NewReader(stream)
	resp, err := http.ReadResponse(body, r)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// copy http header.
	for k, v := range resp.Header {
		for _, s := range v {
			w.Header().Add(k, s)
		}
	}

	// write http header status code.
	w.WriteHeader(resp.StatusCode)
	// copy response body to w.
	io.Copy(w, resp.Body)
	// close response body.
	resp.Body.Close()
}

func (h *httpClient) Close() error {
	if h.listener != nil {
		h.listener.Close()
	}
	if h.host != nil {
		h.host.Close()
	}
	h.logger.Info("service stop")
	return nil
}
