/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package peanut

import (
	"context"
	"github.com/libp2p/go-libp2p-core/host"
	"net"
	"testing"
	"x6t.io/peanut/mock"
)

func NewPeerAddressMock(address string) PeerInfo {
	p, err := PeerAddress(address).Peer()
	if err != nil {
		panic(err)
	}
	return p
}

func Test_httpService_Open(t *testing.T) {
	type fields struct {
		ctx      context.Context
		host     host.Host
		pid      PeerInfo
		pro      Protocol
		address  string
		listener net.Listener
		logger   Logger
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "HTTP Proxy",
			fields: fields{
				ctx:     context.Background(),
				host:    mock.NewHostMock(8092),
				pid:     NewPeerAddressMock("/ip4/127.0.0.1/tcp/12000/ipfs/QmTWRwBhdDg2hpgmKMFXVpSQaaRM9VWUy3ywDdM2Cyr6To"),
				pro:     Proxy,
				address: ":8091",
				logger:  NewDefault(),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &httpClient{
				ctx:      tt.fields.ctx,
				host:     tt.fields.host,
				pid:      tt.fields.pid,
				pro:      tt.fields.pro,
				address:  tt.fields.address,
				listener: tt.fields.listener,
				logger:   tt.fields.logger,
			}
			h.WithLogger(h.logger)
			if err := h.Open(); (err != nil) != tt.wantErr {
				t.Errorf("Open() error = %v, wantErr %v", err, tt.wantErr)
			}

			select {}
		})
	}
}
