/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package mock

import (
	"fmt"
	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-core/host"
	ma "github.com/multiformats/go-multiaddr"
)

func NewHostMock(port int) host.Host {
	addr := fmt.Sprintf("/ip4/0.0.0.0/tcp/%d", port)
	addrStr := libp2p.ListenAddrStrings(addr)
	h, err := libp2p.New(addrStr)
	if err != nil {
		panic(fmt.Errorf("libp2p new host err: %s", err))
	}
	return h
}

func NewMaMock(port int) ma.Multiaddr {
	addr := fmt.Sprintf("/ip4/0.0.0.0/tcp/%d", port)
	m, err := ma.NewMultiaddr(addr)
	if err != nil {
		panic(fmt.Errorf("new ma multi err: %s", err))
	}
	return m
}
