/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package peanut

import (
	"bufio"
	"fmt"
	"github.com/libp2p/go-libp2p-core/network"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/protocol"
	ma "github.com/multiformats/go-multiaddr"
	"io"
	"net/http"
	"os"
	"strings"
)

type Protocol int8

const (
	Proxy Protocol = iota + 1
	NAT
)

// Version protocol version.
const Version = "1.1.0"

func (p Protocol) String() string {
	switch p {
	case Proxy:
		return "/proxy-workpieces"
	case NAT:
		return "/nat-workpieces"
	default:
		panic("libp2p protocol does not exist")
	}
}

// PID get protocol id.
func (p Protocol) PID() protocol.ID {
	s := make([]string, 0, 2)
	s = append(s, p.String())
	s = append(s, Version)
	return protocol.ID(strings.Join(s, string(os.PathSeparator)))
}

// Logger represents an abstracted structured logging implementation. It
// provides methods to trigger log messages at various alert levels and a
// WithField method to set keys for a structured log message.
type Logger interface {
	Debug(...interface{})
	Info(...interface{})
	Trace(...interface{})
	Error(...interface{})
	Fatal(...interface{})
	Panic(...interface{})

	WithField(string, interface{}) Logger

	// Writer Logger can be transformed into an io.Writer.
	// That writer is the end of an io.Pipe and it is your responsibility to close it.
	Writer() *io.PipeWriter
}

type Server interface {
	Open() error
	Close() error
	WithLogger(log Logger)
}

type PeerInfo struct {
	// Pid (the value (if any) following the specified protocol).
	Pid string
	// PeerID ( ID is a libp2p peer identity).
	PeerID peer.ID
	// Address ip4/<a.b.c.d>/ipfs/<peer> becomes /ip4/<a.b.c.d>
	Address ma.Multiaddr
}

// PeerAddress libp2p node address ip.
type PeerAddress string

// Peer get libp2p peer info based on node address ip.
func (addr PeerAddress) Peer() (PeerInfo, error) {
	ipfsaddr, err := ma.NewMultiaddr(string(addr))
	if err != nil {
		return PeerInfo{}, err
	}

	pid, err := ipfsaddr.ValueForProtocol(ma.P_IPFS)
	if err != nil {
		return PeerInfo{}, err
	}

	peerid, err := peer.Decode(pid)
	if err != nil {
		return PeerInfo{}, err
	}

	peerAddr, err := ma.NewMultiaddr(fmt.Sprintf("/ipfs/%s", peer.Encode(peerid)))
	if err != nil {
		return PeerInfo{}, err
	}

	return PeerInfo{
		Pid:     pid,
		PeerID:  peerid,
		Address: ipfsaddr.Decapsulate(peerAddr),
	}, nil
}

func StreamHandler(stream network.Stream) {
	defer stream.Reset()
	defer stream.Close()

	// Create a new buffered reader, as ReadRequest needs one.
	// The buffered reader reads from our stream, on which we
	// have sent the HTTP request (see ServeHTTP())
	body := bufio.NewReader(stream)

	// Read the HTTP request from the buffer
	request, err := http.ReadRequest(body)
	if err != nil {
		return
	}
	defer request.Body.Close()

	request.URL.Scheme = "http"
	h := strings.Split(request.Host, ":")
	if len(h) > 1 && h[1] == "443" {
		request.URL.Scheme = "https"
	}

	request.URL.Host = request.Host
	outReq := new(http.Request)
	*outReq = *request

	resp, err := http.DefaultTransport.RoundTrip(outReq)
	if err != nil {
		return
	}

	if err := resp.Write(stream); err != nil {
		return
	}
}
